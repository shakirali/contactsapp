//
//  DataDownloader.swift
//  BabylonApp
//
//  Created by Shakir Ali on 09/02/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import Foundation
import UIKit


enum DownloadDataError : ErrorType {
    case DownloadError
}

enum Result<T, E> {
    case Success(T)
    case Failure(E)
}

typealias DownloadCompletionBlock = (Result<NSData, DownloadDataError>) -> Void

class DataDownloader {
    
    let urlString: String
    var sessionDataTask : NSURLSessionDataTask?
    
    init(urlString: String) {
        self.urlString = urlString
    }
    
    deinit {
        self.sessionDataTask?.cancel()
    }
    
    func downloadData(completionBlock: DownloadCompletionBlock) {
        //url
        guard let url = NSURL(string: self.urlString) else {
            print("error in url ")
            return
        }
        
        let URLRequest = NSURLRequest(URL: url)
        
        self.sessionDataTask = NSURLSession.sharedSession().dataTaskWithRequest(URLRequest) {
            data, response, error in
            
            guard let httpResponse = response as? NSHTTPURLResponse else { return }
            
            if httpResponse.statusCode == 200 {
                guard let data = data else {
                    completionBlock(Result.Failure(.DownloadError))
                    return
                }
                completionBlock(Result.Success(data))
            }
            else {
                completionBlock(Result.Failure(.DownloadError))
            }
        }
        self.sessionDataTask?.resume()
    }
}
