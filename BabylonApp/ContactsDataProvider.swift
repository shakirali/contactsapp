//
//  ContactsDownloader.swift
//  BabylonApp
//
//  Created by Shakir Ali on 09/02/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import Foundation
import CoreData

protocol ContactsDataProviderDelegate: class {
    func contactsDataProviderDidFinishProcessingData(contactsDataProvider: ContactsDataProvider)
    func contactsDataProviderDidStartDownloadingData(contactsDataProvider: ContactsDataProvider)
    func contactsDataProviderDidFail(contactsDownloader: ContactsDataProvider)
}


class ContactsDataProvider {
    let urlString = "http://fast-gorge.herokuapp.com/contacts";
    var dataDownloader: DataDownloader?
    var mainManagedObjectContext: NSManagedObjectContext
    var managedObjectContext: NSManagedObjectContext
    weak var delegate: ContactsDataProviderDelegate?
    
    
    init(managedObjectContext: NSManagedObjectContext) {
        self.mainManagedObjectContext = managedObjectContext
        self.managedObjectContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        self.managedObjectContext.parentContext = self.mainManagedObjectContext
    }
    
    func downloadData() {
        self.dataDownloader = DataDownloader(urlString: self.urlString)
        self.dataDownloader?.downloadData { [weak self] result in
            switch result {
            case .Success(let data):
                self?.parseJSONData(data)
            case .Failure(let error):
                self?.handleDownloadError(error)
            }
        }
    }
    
    func handleDownloadError(error: DownloadDataError) {
        self.delegate?.contactsDataProviderDidFail(self)
    }
    
    func parseJSONData(data: NSData) {
        do {
            guard let contacts = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as? [AnyObject] else { return }
            
            for contactJSONObj in contacts {
                if let contactJSONDict = contactJSONObj as? [String:AnyObject] {
                    if let  contact = Contact.insertContactJSONDictIntoContext(contactJSONDict, context: self.managedObjectContext) {
                        print(contact)
                    }
                }
            }
            self.managedObjectContext.saveBackgroundContext()
        }catch(let error) {
            print(error)
        }
    }
}