//
//  Contact+CoreDataProperties.swift
//  BabylonApp
//
//  Created by Shakir Ali on 10/02/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Contact {

    @NSManaged var address: String?
    @NSManaged var contactId: NSNumber?
    @NSManaged var createdAt: NSDate?
    @NSManaged var email: String?
    @NSManaged var firstName: String?
    @NSManaged var phoneNumber: String?
    @NSManaged var surname: String?
    @NSManaged var updatedAt: NSDate?
    @NSManaged var adorableImage: NSData?
}
