//
//  ContactCell.swift
//  BabylonApp
//
//  Created by Shakir Ali on 10/02/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var fullNameLbl: UILabel!
    var contact: Contact?
    let adorableURLString = "http://api.adorable.io/avatar/"
    var dataDownloader: DataDownloader?
    
    func configureWithContact(contact: Contact) {
        self.contact = contact
        self.fullNameLbl.text = contact.fullName
        
        if let image = self.contact?.getAdorableUIImage() {
            self.avatar.image = image
        } else {
            guard let email = self.contact?.email where !email.isEmpty else { return }
            self.setupAvatarForURL(self.adorableURLString + email)
        }
    }
    
    deinit {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    func setupAvatarForURL(urlString: String) {

        UIApplication.sharedApplication().networkActivityIndicatorVisible = false

        self.dataDownloader = DataDownloader(urlString: urlString)
        self.dataDownloader?.downloadData { [weak self] result in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            switch result {
            case .Success(let data):
                dispatch_async(dispatch_get_main_queue()) {
                    if let image = UIImage(data: data) {
                        self?.avatar.image = image
                        //self?.contact?.adorableImage = image
                        self?.contact?.storeAdorableImage(image)
                        try! self?.contact?.managedObjectContext?.save()
                    }
                }
            case .Failure(let error):
                //no need to display error to user as it is just a problem with image download. The actual content has already been downloaded.In production App should be logged using services like Raygun Logger.
                print(error)
            }
        }
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }

}
