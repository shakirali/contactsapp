//
//  ContactViewController.swift
//  BabylonApp
//
//  Created by Shakir Ali on 10/02/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import UIKit
import CoreData

class ContactViewController: UIViewController {
    
    @IBOutlet weak var fullNameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var contactIdLbl: UILabel!
    @IBOutlet weak var createdAtLbl: UILabel!
    @IBOutlet weak var updatedAtLbl: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var contact: Contact?
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if let contact = self.contact {
            self.configureViewWithContact(contact)
        }
    }

    func configureViewWithContact(contact: Contact){
        self.fullNameLbl.text = contact.fullName ?? ""
        self.addressLbl.text = contact.address ?? ""
        self.phoneNumberLbl.text = contact.phoneNumber ?? ""
        self.emailLbl.text = contact.email ?? ""
        if let contactId = self.contact?.contactId {
            self.contactIdLbl.text = String(contactId)
        }
        let formatter = NSDateFormatter()
        formatter.dateStyle = .MediumStyle
        formatter.timeStyle = .ShortStyle
        
        if let createdAt = contact.createdAt {
            self.createdAtLbl.text = formatter.stringFromDate(createdAt)
        }
        if let updatedAt = contact.updatedAt {
            self.updatedAtLbl.text = formatter.stringFromDate(updatedAt)
        }
        
        if let image = self.contact?.getAdorableUIImage() {
            self.imageView.image = image
        }
    }
}
