//
//  ManagedObjectContext+BabylonApp.swift
//  BabylonApp
//
//  Created by Shakir Ali on 10/02/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import Foundation

import CoreData

extension NSManagedObjectContext {

    func saveBackgroundContext() {
        guard let mainContext = self.parentContext else { return }
        
        if (!mainContext.hasChanges && !self.hasChanges) {
            return
        }
        
        self.performBlock {
            do {
                try self.save()
                mainContext.performBlock {
                    do {
                        try mainContext.save()
                    } catch let error as NSError {
                        print("Error: \(error.domain) \(error.description)")
                    }
                }
            } catch let error as NSError {
                print("Error: \(error.domain) \(error.description)")
            }
        }
    }
}