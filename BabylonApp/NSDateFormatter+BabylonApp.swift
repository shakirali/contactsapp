//
//  NSDateFormatter+BabylonApp.swift
//  BabylonApp
//
//  Created by Shakir Ali on 10/02/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import Foundation

extension NSDateFormatter {
    private static let RFC3339Formatter: NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        return dateFormatter
    }()
}