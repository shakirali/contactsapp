//
//  ViewController.swift
//  BabylonApp
//
//  Created by Shakir Ali on 09/02/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import UIKit
import CoreData

class ContactsViewController: UIViewController, ManagedObjectContextSettable, NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var managedObjectContext: NSManagedObjectContext!
    lazy var contactsDataProvider: ContactsDataProvider = {
        return ContactsDataProvider(managedObjectContext: self.managedObjectContext)
    }()
    lazy var fetchedResultsController: NSFetchedResultsController = {
        let contactRequest = NSFetchRequest(entityName: "Contact")
        contactRequest.returnsObjectsAsFaults = false
        contactRequest.fetchBatchSize = 20
        let sortDescriptor = NSSortDescriptor(key: "firstName", ascending: true)
        contactRequest.sortDescriptors = [sortDescriptor]
        let frc = NSFetchedResultsController(fetchRequest: contactRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: "contactsdata")
        frc.delegate = self
        return frc
    }()
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        
        do {
            try fetchedResultsController.performFetch()
            if fetchedResultsController.fetchedObjects?.count == 0 {
               self.contactsDataProvider.downloadData()
            }
        }catch {
            print("An error occurred")
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
    }
    
    func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }

    
    //MARK: Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier where identifier == "DisplayContactDetail", let selectedCell = sender as? UITableViewCell, let cellIndex = self.tableView.indexPathForCell(selectedCell) {
            let destinationVC = segue.destinationViewController as? ContactViewController
            guard let contact = self.fetchedResultsController.objectAtIndexPath(cellIndex) as? Contact else { return }
            destinationVC?.contact = contact
        }
    }

    
    //MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        guard let sections = fetchedResultsController.sections  else { return 0 }
        return sections.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController.sections else { return 0 }
        let currentSection = sections[section]
        return currentSection.numberOfObjects
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ContactCell", forIndexPath: indexPath) as! ContactCell
        guard let contact = self.fetchedResultsController.objectAtIndexPath(indexPath) as? Contact else { return cell }
        cell.configureWithContact(contact)
        return cell
    }
    
    //MARK: NSFetchedResultsControllerDelegate
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        if type == .Insert {
            self.tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        }
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
}

