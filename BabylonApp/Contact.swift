//
//  Contact.swift
//  BabylonApp
//
//  Created by Shakir Ali on 09/02/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import UIKit
import CoreData

class Contact: NSManagedObject {
    
    //var adorableImage: UIImage?
    
    private static let RFC3339Formatter: NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        return dateFormatter
    }()
    
    lazy var fullName: String = {
        var name = ""
        if let firstName = self.firstName {
            name = firstName
        }
        if let surname = self.surname {
            name.appendContentsOf(" ")
            name.appendContentsOf(surname)
        }
        return name
    }()

    static func insertContactJSONDictIntoContext(jsonDict: [String:AnyObject], context: NSManagedObjectContext) -> Contact? {
        guard let contact = NSEntityDescription.insertNewObjectForEntityForName("Contact", inManagedObjectContext: context) as? Contact else { return nil }
        
        if let contactId = jsonDict["id"] as? Int {
            contact.setValue(contactId, forKey: "contactId")
        }
        
        if let surname = jsonDict["surname"] as? String {
            contact.setValue(surname, forKey: "surname")
        }
        
        if let firstName = jsonDict["first_name"] as? String {
            contact.setValue(firstName, forKey: "firstName")
        }
        
        if let address = jsonDict["address"] as? String {
            contact.setValue(address, forKey: "address")
        }
        
        if let email = jsonDict["email"] as? String {
            contact.setValue(email, forKey: "email")
        }
        
        if let phoneNumber = jsonDict["phone_number"] as? String {
            contact.setValue(phoneNumber, forKey: "phoneNumber")
        }
        
        if let createdAtStr = jsonDict["createdAt"] as? String {
            contact.setValue(RFC3339Formatter.dateFromString(createdAtStr), forKey: "createdAt")
        }
        
        if let updatedAtStr = jsonDict["updatedAt"] as? String {
            contact.setValue(RFC3339Formatter.dateFromString(updatedAtStr), forKey: "updatedAt")
        }
        return contact
    }
    
    func storeAdorableImage(image: UIImage) {
        self.willChangeValueForKey("adorableImage")
        self.setValue(UIImageJPEGRepresentation(image, 0.5), forKey: "adorableImage")
        self.didChangeValueForKey("adorableImage")
    }
    
    func getAdorableUIImage() -> UIImage? {
        if let imageData = self.valueForKey("adorableImage") as? NSData {
            return UIImage(data: imageData)
        }
        return nil
    }
}
