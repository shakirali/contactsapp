//
//  ConfigurationViewController.swift
//  BabylonApp
//
//  Created by Shakir Ali on 10/02/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import UIKit
import CoreData

class ConfigurationViewController: UIViewController {
    
    var persistenceController: PersistenceStoreController?
    var managedObjectContext: NSManagedObjectContext?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.persistenceController = PersistenceStoreController() { managedObjectContext in
            self.managedObjectContext = managedObjectContext
            self.performSegueWithIdentifier("DisplayContacts", sender: self)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "DisplayContacts" {
            if let destVC = segue.destinationViewController as? ManagedObjectContextSettable {
                destVC.managedObjectContext = self.managedObjectContext
            }
        }
    }

}
