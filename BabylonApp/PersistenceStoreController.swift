//
//  PersistentStoreController.swift
//  BabylonApp
//
//  Created by Shakir Ali on 09/02/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import Foundation
import CoreData

typealias CallBack = (NSManagedObjectContext) -> Void

class PersistenceStoreController {
    
    var callback: ((NSManagedObjectContext) -> Void)? //CallBack?
    private(set) var managedObjectContext: NSManagedObjectContext!
    
    init(callback: CallBack?) {
        self.callback = callback
        self.setupCoreData()
    }
   
    //MARK: CoreDataStack
    private func setupCoreData() {
        
        if self.managedObjectContext != nil { return }
        
        guard let modelURL = NSBundle.mainBundle().URLForResource("contacts", withExtension: "momd") , let mom = NSManagedObjectModel(contentsOfURL: modelURL) else { return }
        
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        
        self.managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        self.managedObjectContext.persistentStoreCoordinator = psc
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            let options = [ NSMigratePersistentStoresAutomaticallyOption : true, NSInferMappingModelAutomaticallyOption : true, NSSQLitePragmasOption : ["journal_mode" : "DELETE"]]
            guard let documentURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).last else { return }
            
            let storeURL = documentURL.URLByAppendingPathComponent("contacts.sqlite")
            try! psc.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: options)
            
            guard let callback = self.callback else { return }
        
            //TODO work in progress. Use configuration view controller.
            dispatch_sync(dispatch_get_main_queue(), {
                callback(self.managedObjectContext)
            })
        })
    }
}
